<h1 align="center">Welcome to ASRA 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.0.1-blue.svg?cacheSeconds=2592000" />
  <a href="https://aasra-se.herokuapp.com/" target="_blank">
  <img alt="Live" src=" https://img.shields.io/badge/Check%20out%20the%20live%20site%20!-blue" />
  </a>
</p>

> A orphanage management system for Yadvendra Puran Niketan(Girls Orphanage in Patiala)

Proposed DFD:
![](./Screenshot_from_2021-10-15_13-59-43.png)

## Install

```sh
 pip install -r requirements.txt 
```

## Usage

```sh
python manage.py runserver
```

## Author

👤 **Sabhya Grover**

* Twitter: [@GroverSabhya](https://twitter.com/GroverSabhya)
* Github: [@SabhyaGrover](https://github.com/SabhyaGrover)
* LinkedIn: [@sabhyagrover](https://linkedin.com/in/sabhyagrover)

## Show your support

Give a ⭐️ if this project helped you!

